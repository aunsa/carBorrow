package com.example.carborrow.model;

import java.math.BigDecimal;

public class Vip {

    //none：0，>=0
    private int level;

    private BigDecimal vipPrice;

    //<=1
    private BigDecimal discountRate;


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }
}
