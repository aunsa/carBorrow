package com.example.carborrow.model;

import java.math.BigDecimal;



public class Customer {
    private int customerId;
    private String name;
    private String phone;

    private String password;

    //none：0，>=0
    private int vipLevel;

    //vipLevel => rate  <=1
    private BigDecimal discountRate;
    private BigDecimal pays;
    private BigDecimal balance;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getVipLevel() {
        return vipLevel;
    }

    public void setVipLevel(int vipLevel) {
        this.vipLevel = vipLevel;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getPays() {
        return pays;
    }

    public void setPays(BigDecimal pays) {
        this.pays = pays;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
