package com.example.carborrow.service;


import com.example.carborrow.dao.BorrowRecordDao;
import com.example.carborrow.model.BRecord;
import com.example.carborrow.model.BorrowRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class BorrowRecordService {

    private final BorrowRecordDao dao;
    private final CarService carService;
    private final CustomerService customerService;

    @Autowired
    public BorrowRecordService(BorrowRecordDao dao, CarService carService, CarService carService1, CustomerService customerService) {
        this.dao = dao;
        this.carService = carService1;
        this.customerService = customerService;
    }


    public List<BorrowRecord> getAllBorrowRecords() {
        return  dao.getAllBorrowRecords();
    }
    public List<BorrowRecord> getAllBorrowRecordsOrder() {
        return dao.getAllBorrowRecordsOrder();
    }

    public List<BRecord> getAllBorrowRecordsCusCar() {
        return dao.getAllBorrowRecordsCusCar();
    }




    public List<BorrowRecord> getBorrowRecordsByS(int status) {
        return  dao.getBorrowRecordsByS(status);
    }
    public List<BorrowRecord> getBorrowRecordsByCusId(int id) {
        return dao.getBorrowRecordsByCusId(id);
    }

    public List<BorrowRecord> getBorrowRecordsByCusIdOrder(int id) {
        return dao.getBorrowRecordsByCusIdOrder(id);
    }


    public List<BorrowRecord> getBorrowRecordsByCusIdState(int cusId ,int state) {
        return dao.getBorrowRecordsByCusIdState(cusId,state);
    }

    public List<BorrowRecord> searchBorrowRecordOrder(String searchWord,int cusId) {
        return dao.searchBorrowRecordOrder(searchWord,cusId);
    }


    public BorrowRecord getBorrowRecordByBDATE(Date bdate) {
        return dao.getBorrowRecordByBDATE(bdate);
    }



    public int getCarIdByRecordId(int recordId) {
        return dao.getCarIdByRecordId(recordId);
    }


    //添加记录 ，同时将car.status=0(借出)
    @Transactional
    public boolean addBorrowRecord(int carId, int customerId , String borrowDate , String returnDate, BigDecimal subAmount) {
        if(dao.addBorrowRecord(carId,customerId, borrowDate, returnDate)>0) {
            if(!carService.updateCarStatus(0,carId)) {
                throw new RuntimeException("Failed to insert borrow record.");
            }
            else if(customerService.subCustomerPaysBalance(customerId,subAmount)) {
                return true;
            }
            else throw new RuntimeException("Failed to insert borrow record.");

        }
        else throw new RuntimeException("Failed to insert borrow record.");

    }


    public boolean updateBorrowRecord(BorrowRecord brecord) {
        return dao.updateBorrowRecord(brecord) >0;
    }

    //更新记录状态 ,同时将car.status=1(可借)
    @Transactional
    public boolean updateBorrowRecordState(int state,int recordId) {
        if(dao.updateBorrowRecordState(state,recordId)>0) {
            if(state==1){
                int carId = getCarIdByRecordId(recordId);
                if(!carService.updateCarStatus(1,carId))
                {
                    throw new RuntimeException("Failed to update borrow record.");
                }
                else return true;
            }
            else throw new RuntimeException("Failed to update borrow record.");

        }
        else throw new RuntimeException("Failed to update borrow record.");
    }




    public boolean deleteBorrowRecord(int brecordId) {
        return  dao.deleteBorrowRecord(brecordId) >0;
    }




}
