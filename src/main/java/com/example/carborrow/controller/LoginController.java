package com.example.carborrow.controller;


import com.example.carborrow.model.Admin;
import com.example.carborrow.model.Customer;
import com.example.carborrow.service.AdminService;
import com.example.carborrow.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/login")
public class LoginController {

    private final AdminService adminService;
    private final CustomerService customerService;

    @Autowired
    public LoginController(AdminService adminService, CustomerService customerService) {
        this.adminService = adminService;
        this.customerService = customerService;
    }

    @PostMapping("/admin")
    public  ResponseEntity<String> adminLogin(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Admin a = adminService.getAdminByName(username);
        if(a== null || !a.getPassword().equals(password)) {
            request.setAttribute("errorMessage", "用户名或密码错误，请重新输入");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("sign in failed!");
        }
        else {

            request.getSession().setAttribute("loginType","admin");
            request.getSession().setAttribute("user",a);
            return ResponseEntity.ok("/cars/admin_home");
        }

    }

    @PostMapping("/customer")
    public  ResponseEntity<String> customerLogin(HttpServletRequest request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Customer cus = customerService.getCusByPhone(username);

        if(cus== null || !cus.getPassword().equals(password)) {
            request.setAttribute("errorMessage", "用户名或密码错误，请重新输入");

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("sign in failed!");
        }
        else {
            request.getSession().setAttribute("loginType","customer");
            request.getSession().setAttribute("user",cus);
            return ResponseEntity.ok("/cars/customer_home");
        }

    }
}


//    @PostMapping("/login/admin")
//    public String login(@RequestParam("username") String username,
//                        @RequestParam("password") String password,
//                        RedirectAttributes redirectAttributes) {
//
//
//        if (adminValid(username, password)) {
//            // 登录成功，将用户信息添加到重定向的 URL 中
//            redirectAttributes.addAttribute("user", username);
////            return ResponseEntity.ok("Login success");
//            return "redirect:/home.html"; // 重定向到主页
//        } else {
//            // 登录失败，将错误信息存储在 RedirectAttributes 中
//            redirectAttributes.addFlashAttribute("errorMessage", "用户名或密码错误，请重新输入");
////            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login failed");
//            return "redirect:/login.html";
//        }
//    }
//    @PostMapping("/login/admin")
//    public ResponseEntity<String> adminLogin(@RequestBody Map<String, String> request) {
//        String username = request.get("username");
//        String password = request.get("password");
//
//        // 处理登录逻辑，验证用户名和密码是否正确
//        if (adminValid(username, password)) {
//            return ResponseEntity.ok("redirect:/home.html");
//        } else {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("redirect:/login.html");
//        }
//    }



