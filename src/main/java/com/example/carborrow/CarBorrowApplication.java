package com.example.carborrow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarBorrowApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarBorrowApplication.class, args);
    }

}
