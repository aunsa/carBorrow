package com.example.carborrow.controller;


import com.example.carborrow.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api")
public class SignUpController {

    private final CustomerService customerService;

    @Autowired
    public SignUpController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/signUp")
    public ResponseEntity<String> SignUp(HttpServletRequest request) {
        String name = request.getParameter("name");
        String phoneNumber = request.getParameter("phone");
        String password = request.getParameter("password");


        try {
            if (customerService.getCusByPhone(phoneNumber) != null) {
                return ResponseEntity.badRequest().body("e01:注册失败，该手机号已注册");
            }

            if (customerService.addCustomer(name,phoneNumber,password)) {
                return ResponseEntity.ok("/login.html");
            }
            else {
                return ResponseEntity.badRequest().body("注册失败！");
            }

        }
        catch (Exception e)
        {

            Logger logger = LoggerFactory.getLogger(getClass());
            logger.error("发生异常：", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("服务器内部错误");
        }




    }

}

