package com.example.carborrow.service;


import com.example.carborrow.dao.CarDao;
import com.example.carborrow.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CarService {
    private  final CarDao carDao  ;

    @Autowired
    public CarService(CarDao carDao) {
        this.carDao = carDao;
    }


    /**
     * 按品牌或车型模糊查询
     */
    public List<Car> getCarsByBM(String searchWord) {

        return carDao.getCarsByBM(searchWord);
    }

    public List<Car> getCarsByBMOrderByStatus(String searchWord) {
        return carDao.getCarsByBMOrderByStatus(searchWord);
    }

    public List<Car> getAllCars() {
        return carDao.getAllCars();
    }
    public List<Car> getAllCarsOrderBySTATUS() {
        return carDao.getAllCarsOrderBySTATUS();
    }

    public List<Car> getCarsByStatus(int status) {
        return carDao.getCarsByStatus(status);
    }
    public Car getCarByLicensePlate(String licensePlate) {
        return carDao.getCarByLicensePlate(licensePlate);
    }



    public boolean addCar(String carBrand , String carModel , String licensePlate , String color , String price  , int status ){
        return carDao.addCar( carBrand, carModel, licensePlate, color, price,status)>0;
    }



    public boolean updateCar(int carId , String carBrand , String carModel , String licensePlate , String color , BigDecimal price  , int status ){
        return carDao.updateCar(carId, carBrand, carModel, licensePlate, color, price,status)>0;
    }

    // 更新汽车状态
    public boolean updateCarStatus(int status,int carId) {
        return carDao.updateCarStatus(status,carId) >0;
    }


    public boolean deleteCar(int carId) {
        return carDao.deleteCar(carId) >0;
    }


}
