package com.example.carborrow.controller;


import com.example.carborrow.model.BRecord;
import com.example.carborrow.model.BorrowRecord;
import com.example.carborrow.model.Car;
import com.example.carborrow.model.Customer;
import com.example.carborrow.service.BorrowRecordService;
import com.example.carborrow.service.CarService;
import com.example.carborrow.service.CustomerService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/cars")
public class CustomerController {

    private final CustomerService customerService;
    private final CarService carService;
    private final BorrowRecordService borrowRecordService;

    @Autowired
    public CustomerController(CustomerService customerService, CarService carService, BorrowRecordService borrowRecordService) {
        this.customerService = customerService;
        this.carService = carService;
        this.borrowRecordService = borrowRecordService;
    }

    @GetMapping("/customer_home")
    public String toHome() {
            return "customer_home";
    }


    @GetMapping("/loggedOut")
    public void loggedOut(HttpServletRequest request) {
        request.getSession().invalidate();
    }

    //customer_list
    @GetMapping("/customer_list")
    public String toList(Model model, HttpServletRequest request) {

            Customer cus = (Customer) request.getSession().getAttribute("user");

            List<Car> list= carService.getAllCarsOrderBySTATUS();

            Customer new_cus = customerService.getCusById(cus.getCustomerId());
            model.addAttribute("user",new_cus);
            model.addAttribute("list",list);

            return "customer_list";

    }
    //customer_list.searchBar
    @PostMapping("/customer_list/query")
    public String GetListByCarModel(Model model, HttpServletRequest request) {
        String searchWord = request.getParameter("searchWord");

        Customer cus = (Customer) request.getSession().getAttribute("user");
        List<Car> list= carService.getCarsByBMOrderByStatus(searchWord);

        Customer new_cus = customerService.getCusById(cus.getCustomerId());
        model.addAttribute("user",new_cus);
        model.addAttribute("list",list);

        return "customer_list";
    }


//    private int carId;
//    private int customerId;
//
//    private Date borrowDate;
//    private Date returnDate;
//
//    // 0:not_return ,1:returned
//    private int state;

    //TODO: cus.balance -= ad-pays; ok
    //customer_list.rent
    @PostMapping("/rent")
    public ResponseEntity<String> Rent(Model model, HttpServletRequest request) {
        int carId = Integer.parseInt(request.getParameter("carId")) ;
        String borrowDate = request.getParameter("borrowDate");
        String returnDate = request.getParameter("returnDate");
        BigDecimal pays = new BigDecimal(request.getParameter("pays"));

        Customer cus = (Customer) request.getSession().getAttribute("user");
        int customerId = cus.getCustomerId();

       if( borrowRecordService.addBorrowRecord(carId,customerId,borrowDate,returnDate,pays)) {
           return ResponseEntity.ok("/cars/customer_list");
       }
       else {
           return ResponseEntity.badRequest().body("租借失败");
       }

    }




    //customer_record
    @GetMapping("/customer_record")
    public String toRecord(Model model, HttpServletRequest request) {
        Customer cus = (Customer) request.getSession().getAttribute("user");

        List<BorrowRecord> recordlist= borrowRecordService.getBorrowRecordsByCusIdOrder(cus.getCustomerId());

        model.addAttribute("list",recordlist);

        return "customer_record";
    }

    //customer_record.searchBar
    @PostMapping("/customer_record/query")
    public String GetMyRecordList(Model model, HttpServletRequest request) {
        String searchWord = request.getParameter("searchWord");

        Customer cus = (Customer) request.getSession().getAttribute("user");
        List<BorrowRecord> list= borrowRecordService.searchBorrowRecordOrder(searchWord,cus.getCustomerId());

        model.addAttribute("list",list);

        return "customer_record";
    }

    //customer_return  customerId && state=0(没还)
    @GetMapping("/customer_return")
    public String toReturn(Model model ,HttpServletRequest request) {
        Customer cus = (Customer) request.getSession().getAttribute("user");

        List<BorrowRecord> recordlist= borrowRecordService.getBorrowRecordsByCusIdState(cus.getCustomerId(),0);

        model.addAttribute("list",recordlist);

        return "customer_return";
    }


    //customer_return.returnRequest

    @PostMapping("/return")
    public ResponseEntity<String> carReturn(Model model, HttpServletRequest request) {
        int recordId = Integer.parseInt(request.getParameter("recordId"));
        //0 ->1
        if(borrowRecordService.updateBorrowRecordState(1,recordId)) {
            return ResponseEntity.ok("/cars/customer_return");
        }
        else {
            return ResponseEntity.badRequest().body("归还失败！");
        }



    }

    @GetMapping("/customer_personal")
    public String toPersonal(Model model , HttpServletRequest request) {
        Customer cus = (Customer) request.getSession().getAttribute("user");
        Customer new_cus = customerService.getCusById(cus.getCustomerId());
        model.addAttribute("user",new_cus);
        return "customer_personal";
    }


    @GetMapping("/customer_vip")
    public String toVip() {
        return "customer_vip";
    }


    //TODO: cus.pays += pays;  ok
    //      cus.balance += pays; ok
    //      if cus.pays >= each:vip vipA.price ,set cus.vipLevel=vipA.vipLevel;  ok
    @PostMapping("/vipPays")
    public ResponseEntity<String> vipPays(Model model, HttpServletRequest request) {
        String amount = request.getParameter("amount");
        Customer cus = (Customer)request.getSession().getAttribute("user");
        if(customerService.addCustomerPaysBalance(cus.getCustomerId(),  new BigDecimal(amount) ))
        {
            return ResponseEntity.ok("/cars/customer_vip");
        }
        else {
            return ResponseEntity.ok("充值失败");
        }

    }
















}
