package com.example.carborrow.service;


import com.example.carborrow.dao.VipDao;
import com.example.carborrow.model.Vip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VipService {

    private  final VipDao vipDao;


    @Autowired
    public VipService(VipDao vipDao) {
        this.vipDao = vipDao;
    }


    public List<Vip> getAllVip() {
        return vipDao.getAllVip();
    }


    public Vip getVipByLevel(int level) {
        return vipDao.getVipByLevel(level);
    }


    public boolean addVip(Vip vip) {
        return vipDao.addVip(vip) >0;
    }


    public boolean updateVip(Vip vip) {
        return vipDao.updateVip(vip) >0;
    }


    public boolean deleteVip(int level) {
        return  vipDao.deleteVip(level) >0;
    }







}

