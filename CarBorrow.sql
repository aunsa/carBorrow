create database if not exists carborrow;
use carborrow;


-- drop table if exists  CarBrand;
-- create table CarBrand
-- (
--     brandId int primary key auto_increment,
--     brandName VARCHAR(40)
--
-- );
--
--
-- drop table if exists CarModel;
-- create table CarModel
-- (
--     modelId int primary key auto_increment,
--     modelName varchar(40),
--     brandId int not null,
--     foreign key (brandId) references CarBrand(brandId)
--
-- );


drop table if exists Car;
create table Car (
    carId int primary key  auto_increment,
    carBrand VARCHAR(40),
    carModel varchar(40),
    licensePlate  varchar(10) ,
    color varchar(15),
--     default:可借    //已借出：0，可借：1，维护中：2，
    status int default 1,
    price decimal(10,2)
);

drop table if exists Vip;
create table Vip (
     vipLevel int primary key ,
     price decimal(6,2),
     discountRate decimal(4,2)
);





drop table if exists Customer;
create table Customer (
    customerId int primary key auto_increment,
    name varchar(20),
    phone varchar(11) unique ,
    password varchar(20),
    vipLevel int default 0 ,
    pays decimal(9,2) default 0,
    balance decimal(9,2) default 0 CHECK (balance >= 0),
    foreign key (vipLevel) references Vip(vipLevel)

);



drop table if exists BorrowRecord;
create table BorrowRecord (
  recordId int primary key auto_increment,
  carId int unique ,
  customerId int ,
  borrowDate date,
  returnDate date,
--       // 0:not_return 没还,1:returned 已还
  state int default 0,
  foreign key (carId) references Car(carId),
  foreign key (customerId) references Customer(customerId)

);


drop table if exists Admin;
create table Admin (
  adminId int primary key auto_increment,
  username varchar(20) unique ,
  password varchar(20)
);

# drop table if exists Money;
# create table Money (
#    customerId int primary key ,
#
#    foreign key (customerId) references Customer(customerId)
# );

--
-- create view car_view(carId,carBrand,carModel,licensePlate,color,status,price)
-- as
-- select carId,carBrand.brandName,carModel.modelName,licensePlate,color,status,price
-- from Car,CarBrand,CarModel
-- where Car.modelId=CarModel.modelId and
--       CarBrand.brandId=CarModel.brandId;


create view customer_view(customerId,name,phone,password,vipLevel,discountRate,pays,balance)
as
select Customer.customerId,name,phone,password,Customer.vipLevel,Vip.discountRate,pays,balance
from Customer,Vip
where Customer.vipLevel=Vip.vipLevel ;


insert into Admin(username, password) values ('root','root');

insert into Vip values
                    (0,0,1),
                    (1,300,0.95),
                    (2,900,0.88);

insert into customer_view(name, phone, password) values
    ('黄泽上','13418201857','123456'),
    ('kana','13418201855','123456'),
    ('leee','13418202222','1314520.a');




-- insert into Brand(brandName) values ('奥迪'),('宝马'), ('奔驰'),('大众'),('福特'),('丰田'),
--             ('本田'),('尼桑'),('雷克萨斯'),('奇瑞'),('别克'),('雪佛兰'),('路虎'),('捷豹'),('法拉利'),('玛莎拉蒂');

--   status=  default:可借1    //已借出：0，可借：1，维护中：2，
insert into car(carBrand,carModel,licensePlate,color,price) values
    ('宝马','宝马i3 eDrive 35L','粤RMV670','black',200),
    ('宝马','宝马i3 eDrive 40L','青A25M1B','white',200),
    ('宝马','宝马i3 eDrive 40L','粤T6N577','white',200),
    ('宝马','宝马i5 eDrive 35L','粤T6N577','white',350),
    ('宝马','宝马6系GT 630i','粤T6N577','white',350),
    ('本田','本田英仕派新能源 e:PHEV','甘A478B6','black',100),
    ('奥迪','奥迪Q5 e-tron','川WPD859','grey',100),
    ('玛莎拉蒂','玛莎拉蒂 Ghibli 3.0T Modena', '粤RV0Y08','red',500);

insert into car(carBrand,carModel,licensePlate,color,price,status) values
        ('法拉利','法拉利812 6.5L GTS', '粤B5F7L6','red',400,2);

--       // state = default:0 //0:not_return 没还,1:returned 已还
insert into BorrowRecord(carId, customerId, borrowDate, returnDate) values
                    (1,1,'2023-12-25','2024-1-1'),
                    (4,1,'2023-12-25','2024-12-27'),
                    (2,2,'2023-11-29','2024-1-2');

UPDATE car SET status = 0 WHERE carId = 1;
UPDATE car SET status = 0 WHERE carId = 4;
UPDATE car SET status = 0 WHERE carId = 2;

insert into BorrowRecord(carId, customerId, borrowDate, returnDate ,state) values
        (9,1,'2023-12-26','2023-12-27',1);
UPDATE car SET status = 1 WHERE carId = 9;


# 测试页面滑动
insert into car(carBrand,carModel,licensePlate,color,price) values
                ('1','宝马i3 eDrive 35L','粤RMV670','black',200),
                ('2','宝马i3 eDrive 40L','青A25M1B','white',200),
                ('3','宝马i3 eDrive 40L','粤T6N577','white',200),
                ('4','宝马i5 eDrive 35L','粤T6N577','white',350),
                ('5','宝马6系GT 630i','粤T6N577','white',350),
                ('6','本田英仕派新能源 e:PHEV','甘A478B6','black',100),
                ('7','奥迪Q5 e-tron','川WPD859','grey',100),
                ('8','玛莎拉蒂 Ghibli 3.0T Modena', '粤RV0Y08','red',500);







