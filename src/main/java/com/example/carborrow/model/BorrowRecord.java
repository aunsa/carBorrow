package com.example.carborrow.model;


import java.util.Date;

public class BorrowRecord {
    private int borrowRecordId;
    private int carId;
    private int customerId;

    private Date borrowDate;
    private Date returnDate;

    // 0:not_return 没还,1:returned 已还
    private int state;


    public int getBorrowRecordId() {
        return borrowRecordId;
    }

    public void setBorrowRecordId(int borrowRecordId) {
        this.borrowRecordId = borrowRecordId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
//    public int getBorrowDays() {
//        return borrowDays;
//    }
//
//    public void setBorrowDays(int borrowDays) {
//        this.borrowDays = borrowDays;
//    }
//
//    public int getReturnState() {
//        return returnState;
//    }
//
//    public void setReturnState(int returnState) {
//        this.returnState = returnState;
//    }
}
