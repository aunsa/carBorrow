package com.example.carborrow.model;

import java.math.BigDecimal;


/**
 * status=  default:可借1    //已借出：0，可借：1，维护中：2，
 */

public class Car {
    private int carId;
    private  String carBrand;
    private  String carModel;
//    private int modelId;

    private String licensePlate;
    private String color;

    //已借出：0，可借：1，维护中：2，
    private int status;

    private BigDecimal price;

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
//
//    public int getModelId() {
//        return modelId;
//    }
//
//    public void setModelId(int modelId) {
//        this.modelId = modelId;
//    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }



}
