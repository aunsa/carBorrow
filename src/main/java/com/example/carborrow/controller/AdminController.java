package com.example.carborrow.controller;


import com.example.carborrow.model.*;
import com.example.carborrow.service.AdminService;
import com.example.carborrow.service.BorrowRecordService;
import com.example.carborrow.service.CarService;
import com.example.carborrow.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/cars")
public class AdminController {
    private final AdminService adminService;
    private final CarService carService;
    private final BorrowRecordService borrowRecordService;
    private final CustomerService customerService;
    @Autowired
    public AdminController(AdminService adminService, CarService carService, BorrowRecordService borrowRecordService, CustomerService customerService) {
        this.adminService = adminService;
        this.carService = carService;
        this.borrowRecordService = borrowRecordService;
        this.customerService = customerService;
    }


    @GetMapping("/admin_home")
    public String toHome() {
        return "admin_home";
    }

    //admin_cars
    
    @GetMapping("/admin_cars")
    public String toCarsList(Model model, HttpServletRequest request) {

        Admin admin = (Admin) request.getSession().getAttribute("user");

        List<Car> list= carService.getAllCars();

        Admin new_admin = adminService.getAdminById(admin.getAdminId());
        model.addAttribute("user",new_admin);
        model.addAttribute("list",list);

        return "admin_cars";

    }

    @PostMapping("/updateCar")
    public ResponseEntity<String> updateCars(HttpServletRequest request){
        int carId = Integer.parseInt(request.getParameter("carId"));
        String carBrand = request.getParameter("carBrand");
        String carModel = request.getParameter("carModel");
        String licensePlate = request.getParameter("licensePlate");
        String color = request.getParameter("color");
        BigDecimal price  = new BigDecimal(request.getParameter("price"));
        int status = Integer.parseInt( request.getParameter("status"));

        if( carService.updateCar(carId,carBrand,carModel,licensePlate,color,price,status)) {
            return ResponseEntity.ok("/cars/admin_cars");
        }
        else {
            return ResponseEntity.badRequest().body("租借失败");
        }


    }
    @PostMapping("/delCar")
    public ResponseEntity<String> delCars(HttpServletRequest request){
        int carId = Integer.parseInt(request.getParameter("carId"));

        if( carService.deleteCar(carId)) {
            return ResponseEntity.ok("/cars/admin_cars");
        }
        else {
            return ResponseEntity.badRequest().body("删除失败");
        }

    }
    @PostMapping("/addCar")
    public ResponseEntity<String> addCars(HttpServletRequest request){
        String carBrand = request.getParameter("carBrand");
        String carModel = request.getParameter("carModel");
        String licensePlate = request.getParameter("licensePlate");
        String color = request.getParameter("color");
        String price  = request.getParameter("price");
        int status = Integer.parseInt( request.getParameter("status"));

        if( carService.addCar(carBrand,carModel,licensePlate,color,price,status)) {
            return ResponseEntity.ok("/cars/admin_cars");
        }
        else {
            return ResponseEntity.badRequest().body("添加失败");
        }



    }

    //admin_record
    @GetMapping("/admin_record")
    public String toRecordList(Model model, HttpServletRequest request) {

//        Admin admin = (Admin) request.getSession().getAttribute("user");

        List<BRecord> list= borrowRecordService.getAllBorrowRecordsCusCar();

//        Admin new_admin = adminService.getAdminById(admin.getAdminId());
//        model.addAttribute("user",new_admin);
        model.addAttribute("list",list);

        return "admin_record";

    }

    //admin_record
    @GetMapping("/admin_cus")
    public String toCusList(Model model, HttpServletRequest request) {

        List<Customer> list= customerService.getAllCus();

        model.addAttribute("list",list);

        return "admin_cus";

    }



















}
