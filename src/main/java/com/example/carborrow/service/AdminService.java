package com.example.carborrow.service;


import com.example.carborrow.dao.AdminDao;
import com.example.carborrow.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    private  final AdminDao adminDao;


    @Autowired
    public AdminService(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    public List<Admin> getAllAdmin() {
        return adminDao.getAllAdmin();
    }

    public Admin getAdminByName(String name) {
        return adminDao.getAdminByName(name);
    }

    public Admin getAdminById(int id) {
        return  adminDao.getAdminById(id);
    }


    public boolean addAdmin(Admin admin){
        return adminDao.addAdmin(admin) >0;
    }

    public boolean updateAdminByName(Admin admin) {
        return adminDao.updateAdminByName(admin) >0;
    }

    public boolean updateAdminById(Admin admin) {
        return adminDao.updateAdminById(admin) >0;
    }


    public boolean deleteAdmin(int adminId){
        return  adminDao.deleteAdmin(adminId) >0;
    }





}
